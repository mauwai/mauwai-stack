# mattermost-pupilfirst-stack

A stack with mattermost and pupilfirst docker containers along with local backups and local storage

## How to use

1. Copy settings.env.example to settings.env
2. Edit settings.env, changing the settings to suit
3. Run `./setup.sh` to generate env and config files, as well as secrets
4. Run `docker-compose up -d`
5. Go to `$MATTERMOST_DOMAIN_NAME` and set up an admin user

For pupilfirst its a bit more complicated

6. Execute a shell inside the pupilfirst container: `docker-compose exec pupilfirst bash`
7. Run `bundle exec rails db:schema:load`
8. Run `bundle exec rails db:seed`
9. Run `bundle exec rails console` to open the rails console
10. Enter `user = User.find_by(email: 'admin@example.com')`
11. Enter `user.update!(password: 'somepassword', password_confirmation: 'somepassword')`
12. Enter `School.first.domains.create!(fqdn: '<Your PUPILFIRST_DOMAIN_NAME setting>', primary: true)`
13. Ctrl-D to exit Rails console
14. Run `bundle exec rails assets:precompile` to precompile the assets. Note this will take a while.
15. Restart pupilfirst by running `docker-compose restart pupilfirst pupilfirst-worker pupilfirst-cron`

If we use the admin we created in 10/11 it will result in 500 errors, maybe due to incorrect permissions.

16. Login with the user that you created in steps 10 and 11
17. Create another admin with your actual email address
18. Logout and login with new admin
19. Delete the admin `admin@example.com`

## Other things that require setup

- Email domain validation
- DNS settings
- Cloud hosting