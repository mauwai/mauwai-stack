#!/bin/bash

declare -a GENERATED_ENV

if [[ ! -f "./settings.env" ]]; then
    echo "Error: settings.env not found. Please copy settings.env.example to settings.env and fill out the settings"
    exit 1
fi

GENERATED_ENV=(
    BOOKSTACK_APP_KEY@32
    BOOKSTACK_MARIADB_PASSWORD
    BOOKSTACK_MARIADB_ROOT_PASSWORD
    PUPILFIRST_SECRET_KEY
    PUPILFIRST_POSTGRES_PASSWORD
    MATTERMOST_POSTGRES_PASSWORD
)

generate_password() {
  # generate_password [length]
  # length default is 48
  tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w ${1-48} | head -n 1 | tr -d '\n'
}

touch settings.generated.env
for SETTING in ${GENERATED_ENV[@]}; do
    BITS=($(echo $SETTING | tr '@' '\n'))
    NAME=${BITS[0]}
    KEYLENGTH=${BITS[1]-64}
    if grep -q "^$NAME=" "settings.generated.env"; then
        :  # do nothing
    else
        echo "Generating $NAME in settings.generated.env"
        echo "$NAME=$(generate_password $KEYLENGTH)" >> settings.generated.env
    fi
done

# # load environment into session
set -a
. ./settings.env
. ./settings.generated.env
set +a

for TEMPLATE_PATH in $(find . -iname *.template); do
    RENDERED_PATH=${TEMPLATE_PATH%.template}
    if [[ -f "$RENDERED_PATH" ]]; then
        :  # do nothing
    else
        echo "Generating $RENDERED_PATH..."
        cat $TEMPLATE_PATH | envsubst > $RENDERED_PATH
    fi
done

# setup directories and permissions for directories
sudo mkdir -p config/mattermost
sudo chown 2000:2000 config/mattermost
sudo mkdir -p ./data/mattermost/{data,logs,client-plugins,plugins}
sudo chown -R 2000:2000 ./data/mattermost/{data,logs,client-plugins,plugins}
sudo mkdir -p ./data/bookstack/bookstack/{public,storage}/uploads
sudo chown -R 33:33 ./data/bookstack/bookstack